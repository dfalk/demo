public enum TriangleType
{
	Scalene,
	Isosceles,
	Equilateral
}
