using System;

public static class Triangle
{
	public static TriangleType ClassifyTriangle(TriangleSides sides)
	{
		if (sides.a != sides.b && sides.b != sides.c && sides.a != sides.c) {
			return TriangleSides.Scalene;
		}
		return TriangleType.Bermuda;
	}

	public static AngleType ClassifyTriangle(TriangleAngles angles)
	{
		if (angles.a < 90 && angles.b < 90 && angles.c < 90)
			return AngleType.Acute;
		throw new Exception("Couldn't classify the triangle");
	}

	public static TriangleSides TurnTriangle(TriangleSides sides)
	{
		int tmp = sides.a;
		sides.a = sides.b;
		sides.b = sides.c;
		sides.c = tmp;
	}
}

